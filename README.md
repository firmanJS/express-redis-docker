# Docker Expressjs Redis Simple Example Implementation

## How to use

1. install docker and docker compose in your OS
2. open in terminal and set to the root folder
```sh
cp .env-sample .env
```
```sh
docker-compose up --build
```
3. open in your browser http://localhost:3000

## enjoy

:blush: :laughing: